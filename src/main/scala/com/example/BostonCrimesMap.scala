package com.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

import java.nio.file.{Files, Paths}

object BostonCrimesMap {
  def main(args: Array[String]): Unit = {
    if (args.length != 3) {
      println("Usage: BostonCrimesMap <path to dataset file> <path to crime codes dictionary file> <path to output directory")
      return
    }

    val pathToDataset = args(0)
    val pathToDict = args(1)
    val pathToOutput = args(2)
    val outputFilename = "output.parquet"

    var fileNotFoundFlag = false

    if (!Files.exists(Paths.get(pathToDataset))) {
      fileNotFoundFlag = true
      println("Dataset file not found")
    }

    if (!Files.exists(Paths.get(pathToDict))) {
      fileNotFoundFlag = true
      println("Crime codes file not found")
    }

    if (!Files.exists(Paths.get(pathToOutput))) {
      fileNotFoundFlag = true
      println("Output directory not found")
    }

    if (fileNotFoundFlag) return

    Logger.getLogger("org").setLevel(Level.ERROR)

    val spark = SparkSession
      .builder()
      .appName("Boston Criminal Map")
      //.master("local")
      .getOrCreate()

    import spark.implicits._

    val crimeCodeSchema = Encoders.product[CrimeCode].schema
    val crimeFactSchema = Encoders.product[CrimeFact].schema

    val crimeFactsDS: Dataset[CrimeFact] = spark
      .read
      .option("header", "true")
      .schema(crimeFactSchema)
      .csv(pathToDataset)
      .as[CrimeFact]

    val crimeCodesDS: Dataset[CrimeCode] = spark
      .read
      .option("header", "true")
      .schema(crimeCodeSchema)
      .csv(pathToDict)
      .as[CrimeCode]
      .distinct() // убираем задвоения строк

    val crimeCodesBroadcastDS = broadcast(crimeCodesDS)

    val crimesDS = crimeFactsDS
      .join(
        crimeCodesBroadcastDS,
        'CODE === 'OFFENSE_CODE and 'NAME === 'OFFENSE_DESCRIPTION,
        "inner"
      )

    crimesDS
      .groupBy('DISTRICT, 'YEAR, 'MONTH)
      .agg(
        countDistinct('INCIDENT_NUMBER).as("crimes")
      )
      .createOrReplaceTempView("sub_agg")

    val monthStatDS = spark.sql(
      "select DISTRICT as D1, percentile_approx(crimes, 0.5) as crimes_monthly " +
        "from sub_agg " +
        "group by DISTRICT"
    )

    val frequentCrimes = crimesDS
      .withColumn(
        "crime_type",
        trim(split('OFFENSE_DESCRIPTION, "-").getItem(0))
      )
      .groupBy('DISTRICT, 'crime_type)
      .agg(
        countDistinct('INCIDENT_NUMBER).as("cnt")
      )
      .withColumn(
        "sort_col",
        struct('cnt, 'crime_type.as("descr"))
      )
      .groupBy('DISTRICT.as("D2"))
      .agg(
        concat_ws(
          ", ",
          expr("slice(sort_array(collect_list(sort_col), false).descr, 1, 3)")
        ).as("frequent_crime_types")
      )

    crimesDS
      .groupBy('DISTRICT)
      .agg(
        countDistinct('INCIDENT_NUMBER).as("crimes_total"),
        avg('Lat).as("lat"),
        avg('Long).as("lng")
      )
      .join(
        monthStatDS,
        'DISTRICT === 'D1,
        //crimeFactsDS("DISTRICT") === monthStatDS("DISTRICT"),
        "inner"
      )
      .join(
        frequentCrimes,
        'DISTRICT === 'D2,
        //crimeFactsDS("DISTRICT") === frequentCrimes("DISTRICT"),
        "inner"
      )
      .select(
        'DISTRICT,
        'crimes_total,
        'crimes_monthly,
        'frequent_crime_types,
        'lat,
        'lng
      )
      .write
      .parquet(Paths.get(pathToOutput, outputFilename).toString)

    spark.stop()
  }

}
