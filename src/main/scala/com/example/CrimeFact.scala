package com.example

import java.sql.Timestamp

case class CrimeFact(
                      INCIDENT_NUMBER: String,
                      OFFENSE_CODE: Option[Int],
                      OFFENSE_CODE_GROUP: Option[String],
                      OFFENSE_DESCRIPTION: Option[String],
                      DISTRICT: Option[String],
                      REPORTING_AREA: Option[Int],
                      SHOOTING: Option[String],
                      OCCURRED_ON_DATE: Option[Timestamp],
                      YEAR: Option[Int],
                      MONTH: Option[Int],
                      DAY_OF_WEEK: Option[String],
                      HOUR: Option[Int],
                      UCR_PART: Option[String],
                      STREET: Option[String],
                      Lat: Option[BigDecimal],
                      Long: Option[BigDecimal],
                      Location: Option[String]
                    )
